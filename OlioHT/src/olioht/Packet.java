/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package olioht;

import java.util.ArrayList;

/**
 *
 * @author k8021
 */

/**
 * Basic packetclass, contains info for the route and the item inside the packet
 */

public class Packet {
    private int packetclass;
    private Item item;
    private ArrayList<Double> latlon;
    private boolean breaks;
    private String startPostAddress;
    private String endPostAddress;
    
    public Packet(int packetclass, Item item, ArrayList<Double> latlon, boolean b, String spa, String epa) {
        this.packetclass = packetclass;
        this.item = item;
        this.latlon = latlon;
        breaks = b;
        startPostAddress = spa;
        endPostAddress = epa;
    }
    
    @Override
    public String toString() {
        return item.toString();
    }
    
    public int getPacketClass() {
        return packetclass;
    }
    
    public ArrayList<Double> getLatLon() {
        return latlon;
    }
    
    public boolean getBreaks() {
        return breaks;
    }
    
    public void setPacketClass(int c) {
        packetclass = c;
    }
    
    public Item getItem() {
        return item;
    }

    public String getStartPostAddress() {
        return startPostAddress;
    }

    public String getEndPostAddress() {
        return endPostAddress;
    }
    
    
}

/**
 * Subclasses to represent all the packetclasses
 * 
 */

class FirstClass extends Packet {
    public FirstClass(Item item, ArrayList<Double> latlon, boolean b, String spa, String epa) {
        super(1, item, latlon, b, spa, epa);
    }
    
}

class SecondClass extends Packet {
    public SecondClass(Item item, ArrayList<Double> latlon, boolean b, String spa, String epa) {
        super(2, item, latlon, b, spa, epa);
    }
}

class ThirdClass extends Packet {
    public ThirdClass(Item item, ArrayList<Double> latlon, boolean b, String spa, String epa) {
        super(3, item, latlon, b, spa, epa);
    }
}