/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author f0388819
 */
public class FXMLDocumentController implements Initializable {
    
    private Cities cities;
    private String content;
    private SmartPosts smartposts;
    private Storage storage;
    private BufferedWriter bw;
    private File file;
    
    @FXML
    private WebView webview;
    @FXML
    private Button addCitiesButton;
    @FXML
    private Button createPacketButton;
    @FXML
    private Button removeRoutesButton;
    @FXML
    private Button updatePacketButton;
    @FXML
    private ComboBox<String> cityComboBox;
    @FXML
    private ComboBox<Packet> storageComboBox;
    @FXML
    private Button sendPacketButton;
    @FXML
    private Button errorButton;
    @FXML
    private Label errorLabel;
    @FXML
    private ListView<String> logListView;
    @FXML
    private Label packetCountLabel;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        /*
        Init-function parses cities from xml-data, and creates Singletons for Cities
        SmartPosts and Storage classes
        */
                
        
        try {
            webview.getEngine().load(getClass().getResource("index.html").toExternalForm());
            URL xml = new URL("http://smartpost.ee/fi_apt.xml");

            BufferedReader br = new BufferedReader(new InputStreamReader(xml.openStream()));

            content = "";
            String line;

            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
            CityParser cp = new CityParser(content);
            cities = Cities.getInstance();
            cities.addCities(cp.getCities());
            cityComboBox.setItems(cities.getCities());
            smartposts = SmartPosts.getInstance();
            storage = Storage.getInstance();
            file = new File("log.txt");
            bw = new BufferedWriter(new FileWriter(file, true));
            String timestamp = new SimpleDateFormat("dd.MM.yyyy HH:mm'\t'").format(new Date());
            bw.write("\nUusi istunto aloitettu " + timestamp + "\n");
            bw.close();
            
            /*
            //Premade packets for debugging purposes
            
            ArrayList<Double> latlon = new ArrayList();
            latlon.add(60.1813017);
            latlon.add(24.6781264);
            latlon.add(60.1684243);
            latlon.add(24.9423342);
            storage.addPacket(new FirstClass(new Pants(), latlon, false, "täältä", "tänne"));
            storage.addPacket(new FirstClass(new RubberDuck(), latlon, false, "täältä", "tänne"));
            */
            
        } catch (IOException ex) {
            
        }
    }    

    @FXML
    private void addCitiesAction(ActionEvent event) {
        
        /*
        Calls SmarPostParser, which takes city-name as parameter, and parses all matching smarposts from xml
        Then adds parsed SmartPosts to map. For-loop to check if trying to add duplicate SmartPosts
        */
        Boolean check = false;
        String city = cityComboBox.getValue();
        for (int i = 0; i < smartposts.getSmartPosts().size(); i++) {
            if (smartposts.getSmartPosts().get(i).getCity().equals(city)) {
                check = true;
            }
        }
        if (city != null && check == false) {
            SmartPostParser spp = new SmartPostParser(content,city);
            smartposts.addSmartPosts(spp.getSmartPosts());
            smartposts.addCity(city);
            for (int i = 0; i < spp.getSmartPosts().size(); i++) {
                SmartPost sp = spp.getSmartPosts().get(i);
                String address = sp.getAddress() + "," + sp.getCode() + " " + sp.getCity();
                System.out.println(address);
                String info = sp.getPostoffice() + " " + sp.getAvailability();
                //System.out.print(info);
                webview.getEngine().executeScript("document.goToLocation('"+ address +"', '" + info + "', 'red')");
            }
            
        }
        
    }

    @FXML
    private void createPacketAction(ActionEvent event) throws IOException {
        
        /*
        Opens a new window for creating new packets
        */
        
        Stage sendPacketView = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLMakePacket.fxml"));
        Scene scene = new Scene(page);
        sendPacketView.setScene(scene);
        sendPacketView.show();
    }

    @FXML
    private void removeRoutesAction(ActionEvent event) {
        webview.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void updatePacketAction(ActionEvent event) {
        
        /*
        If a packet can't be sent for any reason, resolves conflicts by changing packetclass to 3,
        and if required, adds extra weight for the item not to break
        */
        
        if (storageComboBox.getValue() != null) {
            storageComboBox.getValue().setPacketClass(3);
            if (storageComboBox.getValue().getItem().getWeight() < 15 && storageComboBox.getValue().getBreaks() == true) {
                storageComboBox.getValue().getItem().setWeight(storageComboBox.getValue().getItem().getWeight() + 15);
                errorLabel.setText("Pakettiin lisättiin 15kg pumpulia lisäpainoksi");
                errorLabel.setVisible(true);
                errorButton.setVisible(true);
            }
        }
    }
    
    @FXML
    private void sendPacketAction(ActionEvent event) throws IOException {
        
        /*
        Try sending selected packet:
        If any conflicts are found in packet parameters, won't send and displays error message.
        If sending is successful draws route to map and writes a log entry to log in second tab
        and writes same info to a file
        Log entries contain: Timestamp, name of item, start address, end address, distance between start and end.
        */
        
        if (storageComboBox.getValue() != null) {
            ArrayList<Double> latlon = storageComboBox.getValue().getLatLon();
            int packetclass = storageComboBox.getValue().getPacketClass();
            String color = "";
            if (packetclass == 1) {
                color = "blue";
            } else if (packetclass == 2) {
                color = "green";
            } else if (packetclass == 3) {
                color = "red";
            }
            double a = latlon.get(0);
            double b = latlon.get(1);
            double c = latlon.get(2);
            double d = latlon.get(3);
            
            String command = "document.createPath([" + a + ", " + b + ", " + c + ", " + d + "], " + "'" + color + "'," + packetclass + ")";
            double distance = (Double)webview.getEngine().executeScript(command);
            if (packetclass == 1 && storageComboBox.getValue().getBreaks() == true) {
                errorLabel.setText("TIMO mies näyttää vihaiselta, särkyvää tavaraa ei kannata lähettää 1. luokassa");
                errorLabel.setVisible(true);
                errorButton.setVisible(true);
                webview.getEngine().executeScript("document.deletePaths()");
            } else if (distance > 150 && packetclass == 1) {
                errorLabel.setText("Ensimmäisen luokan pakettia ei voi lähettää yli 150km päähän");
                errorLabel.setVisible(true);
                errorButton.setVisible(true);
                webview.getEngine().executeScript("document.deletePaths()");
            } else if (packetclass == 3 && storageComboBox.getValue().getBreaks() == true && storageComboBox.getValue().getItem().getWeight() < 15){
                errorLabel.setText("Kolmannen luokan paketti saattaa olla liian kevyt ja saattaa hajota *krhm* jostain syystä, lisää pakettiin vaikka pumpulia esineen suojaksi");
                errorLabel.setVisible(true);
                errorButton.setVisible(true);
                webview.getEngine().executeScript("document.deletePaths()");
            } else {
                Packet p = storageComboBox.getValue();
                String log = p.getItem().toString() + "  " + p.getStartPostAddress() + "  " + p.getEndPostAddress() + "  " + distance + "km";
                System.out.println(log);
                logListView.getItems().add(log);
                packetCountLabel.setText("Pakettien määrä: " + logListView.getItems().size());
                bw = new BufferedWriter(new FileWriter(file, true));
                String timestamp = new SimpleDateFormat("dd.MM.yyyy HH:mm'\t'").format(new Date());
                bw.write(timestamp + log + "\n");
                bw.close();
                
            }
        }
    }

    @FXML
    private void StorageAction(Event event) {
        
        /*
        Opening PacketStorage ComboBox refreshes its content to match list of all created packets
        */
        
        storageComboBox.setItems(Storage.getInstance().getStorage());
    }

    @FXML
    private void errorButtonAction(ActionEvent event) {
        
        /*
        Errors are displayed under map. Error triggers cause a label describing the error and a button to close the message to be set to visible.
        Clicking the OK button sets both label and button to hidden.
        */
        
        errorLabel.setText("");
        errorLabel.setVisible(false);
        errorButton.setVisible(false);
    }
}
