/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author f0388819
 */

public class SmartPosts {
    private static SmartPosts instance = null;
    private ObservableList<SmartPost> smartpostlist;
    private ObservableList<String> cities;

    protected SmartPosts() {
        smartpostlist = FXCollections.observableArrayList();
        cities = FXCollections.observableArrayList();
    }
    
    public static SmartPosts getInstance() {
        if (instance == null) {
            instance = new SmartPosts();
        }
        return instance;
    }
    
    public ObservableList<SmartPost> getSmartPosts() {
        return smartpostlist;
    }

    public ObservableList<String> getCities() {
        return cities;
    }
    
    public void addSmartPosts(ObservableList<SmartPost> smartposts) {
        smartpostlist.addAll(smartposts);
    }
    
    public void addCity(String city) {
        cities.add(city);
        FXCollections.sort(cities);
    }
    
    
}
