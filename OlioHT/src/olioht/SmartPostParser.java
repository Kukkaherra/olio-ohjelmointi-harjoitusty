/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.IOException;
import java.io.StringReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author f0388819
 */
public class SmartPostParser {

    private Document doc;
    private ObservableList<SmartPost> smartposts;


    public SmartPostParser (String content, String cityname) {
        
        /*
        Code from lecture examples, adapted to work for this project
        */
        
        try {
            smartposts = FXCollections.observableArrayList();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            parseCurrentData(cityname);

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println("Errori");
        }

    }

    private void parseCurrentData(String cityname) {
        NodeList nodes = doc.getElementsByTagName("place");

        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String city = getValue("city", e).trim();
            String address = getValue("address", e).trim();
            String code = getValue("code", e).trim();
            double lat = Double.parseDouble(getValue("lat", e));
            double lng = Double.parseDouble(getValue("lng", e));
            String availability = getValue("availability", e).trim();
            String postoffice = getValue("postoffice", e).trim();
            SmartPost sp = new SmartPost(city, code, lat, lng, address, availability, postoffice);
            if (city.equals(cityname)) {
                smartposts.add(sp);
            }
            //System.out.println(name);
            /*
            System.out.println((String)map.get("ID"));
            System.out.println((String)map.get("Name"));
            */

        }



    }

    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();

    }
    
    public ObservableList<SmartPost> getSmartPosts() {
        return smartposts;
    }
}