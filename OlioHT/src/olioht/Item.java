/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package olioht;

/**
 *
 * @author k8021
 */
public class Item {
    private String name;
    private double weight;
    private double size;
    
    public Item (String n, double w, double s) {
        name = n;
        weight = w;
        size = s;
    }
    
    public String toString() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getSize() {
        return size;
    }
    
    //Setter required incase TIMO-dude is frustrated and tries to relieve stress by throwing customers packet around.
    //If case applies, can add cotton, or possibly other items to increase packets weight
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    
    
}

class RubberDuck extends Item {
    public RubberDuck () {
        super("Kuminen Dodo-Sorsa", 0.2, 10*15*10);
    }
}

class Snus extends Item {
    public Snus () {
        super("Odens Cold Dry Portion", 0.1, 5*5*2);
    }
}

class AdultToy extends Item {
    public AdultToy () {
        super("RealGirlfriend - TinaDwarf", 50, 90*55*45);
    }
}

class Pants extends Item {
    public Pants () {
        super("Lederhosen", 1, 10*20*20);
    }
}