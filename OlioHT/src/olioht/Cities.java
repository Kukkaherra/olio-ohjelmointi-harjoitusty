/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author f0388819
 */
public class Cities {
    private static Cities instance = null;
    private ObservableList<String> citylist;

    protected Cities() {
        citylist = FXCollections.observableArrayList();
    }
    
    public static Cities getInstance() {
        if (instance == null) {
            instance = new Cities();
        }
        return instance;
    }
    
    public void addCities(ObservableList<String> cities) {
        citylist.addAll(cities);
    }
    
    public ObservableList<String> getCities() {
        return citylist;
    }
}
