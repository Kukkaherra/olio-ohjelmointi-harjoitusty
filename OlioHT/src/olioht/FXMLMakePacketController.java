/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package olioht;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author k8021
 */
public class FXMLMakePacketController implements Initializable {
    
    private Cities cities;
    private SmartPosts smartposts;
    private Item item;
    private Storage storage;
    
    @FXML
    private ComboBox<String> startComboBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private ComboBox<SmartPost> startPostComboBox;
    @FXML
    private ComboBox<String> endComboBox;
    @FXML
    private ComboBox<SmartPost> endPostComboBox;
    @FXML
    private Button packetInfoButton;
    @FXML
    private Button createPacketButton;
    @FXML
    private Button cancelButton;
    @FXML
    private RadioButton classOne;
    @FXML
    private RadioButton classTwo;
    @FXML
    private RadioButton classThree;
    @FXML
    private CheckBox fragileCheckBox;
    @FXML
    private ComboBox<Item> itemComboBox;
    @FXML
    private Button checkClassButton;
    @FXML
    private Tooltip classTooltip;
    @FXML
    private Label infoLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        /*
        Populating itemlist with premade items, and setting tooltiptext for packetclassinfo.
        Only those cities that have had smarposts added to map are available for selection.
        */
        
        itemComboBox.getItems().add(new RubberDuck());
        itemComboBox.getItems().add(new Snus());
        itemComboBox.getItems().add(new AdultToy());
        itemComboBox.getItems().add(new Pants());
        cities = Cities.getInstance();
        smartposts = SmartPosts.getInstance();
        storage = Storage.getInstance();
        startComboBox.setItems(smartposts.getCities());
        endComboBox.setItems(smartposts.getCities());
        classTooltip.setText("1. luokka\n\tKoko max: 30*30*30cm Paino max: 5kg"
                + "\n\tPaketit voi lähettää max. 150km päähän\n\tPaketit voivat hajota nopeassa kuljetuksessa"
                + "\n2. luokka\n\tKoko max: 20*20*20cm Paino max 2kg"
                + "\n3. luokka\n\tKoko max:100*60*60 Paino max: 100kg\n\tPaketti kannattaa olla yli 15kg painoinen");
        
    }    


    @FXML
    private void setItemAction(Event event) {
        
        /*
        Create Packet window uses item variable to hold the current info about the packet before it's made
        Selecting one of the premade items automatically disables packetclasses if required, none of the premade items are fragile
        */
        
        item = itemComboBox.getValue();
        classOne.setSelected(false);
        classTwo.setSelected(false);
        classThree.setSelected(false);
        if (itemComboBox.getValue() == itemComboBox.getItems().get(0)) {
            classOne.setDisable(false);
            classTwo.setDisable(false);
            classThree.setDisable(false);
        } else if (itemComboBox.getValue() == itemComboBox.getItems().get(1)) {
            classOne.setDisable(false);
            classTwo.setDisable(false);
            classThree.setDisable(false);
        } else if (itemComboBox.getValue() == itemComboBox.getItems().get(2)) {
            classOne.setDisable(true);
            classTwo.setDisable(true);
            classThree.setDisable(false);
        } else if (itemComboBox.getValue() == itemComboBox.getItems().get(3)) {
            classOne.setDisable(false);
            classTwo.setDisable(false);
            classThree.setDisable(false);
        } else {
            classOne.setDisable(false);
            classTwo.setDisable(false);
            classThree.setDisable(false);
        }
    }
    
    @FXML
    private void createPacketAction(ActionEvent event) {
        
        /*
        First check if item exists in current sessio (item has been selected or made).
        If item exists, coordinates and addressess for the packet.
        Check which packetclass is selected and create a packet containing the item
        with chosen parameters.
        */
        
        try {
            if (item != null) {
                Stage stage = (Stage) createPacketButton.getScene().getWindow();
                ArrayList<Double> latlon = new ArrayList();
                latlon.add(startPostComboBox.getValue().getLat());
                latlon.add(startPostComboBox.getValue().getLng());
                latlon.add(endPostComboBox.getValue().getLat());
                latlon.add(endPostComboBox.getValue().getLng());
                String spa = startPostComboBox.getValue().toString();
                String epa = endPostComboBox.getValue().toString();
                boolean b = fragileCheckBox.isSelected();
                if (classOne.isSelected()) {
                    Packet packet = new FirstClass(item, latlon, b, spa, epa);
                    storage.addPacket(packet);
                    stage.close();
                } else if (classTwo.isSelected()) {
                    Packet packet = new SecondClass(item, latlon, b, spa, epa);
                    storage.addPacket(packet);
                    stage.close();
                } else if (classThree.isSelected()) {
                    Packet packet = new ThirdClass(item, latlon, b, spa, epa);
                    storage.addPacket(packet);
                    stage.close();
                }
                
            }
            
            
        } catch (Exception e) {
        
        }
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
    
    /*
    The three following Actions are for packetclass selection functionality.
    Selecting a class removes other selections.
    */
    
    @FXML
    private void classOneAction(ActionEvent event) {
        classOne.setSelected(true);
        classTwo.setSelected(false);
        classThree.setSelected(false);
    }

    @FXML
    private void classTwoAction(ActionEvent event) {
        classOne.setSelected(false);
        classTwo.setSelected(true);
        classThree.setSelected(false);
    }

    @FXML
    private void classThreeAction(ActionEvent event) {
        classOne.setSelected(false);
        classTwo.setSelected(false);
        classThree.setSelected(true);
    }


    @FXML
    private void checkClassAction(ActionEvent event) {
        
        /*
        Check the users parameters for a new item, and gray out packetclasses that
        the item can't be shipped with. If packet parameters don't fit any of the criteria,
        disable all packetclass selections to prevent making of packet.
        */
        
        try {
            double weight = Double.parseDouble(weightField.getText());
            double size = Double.parseDouble(sizeField.getText());
            if ((weight > 0 && weight < 15) && (size > 0 && size < 30*30*30)) {
                classOne.setDisable(false);
            } else {
                classOne.setDisable(true);
                classOne.setSelected(false);
            }
            if ((weight > 0 && weight < 35) && (size > 0 && size < 20*20*20)) {
                classTwo.setDisable(false);
            } else {
                classTwo.setDisable(true);
                classTwo.setSelected(false);
            }
            if ((weight > 0 && weight <= 100) && (size > 0 && size <= 100*60*60)) {
                classThree.setDisable(false);
            } else {
                classThree.setDisable(true);
                classThree.setSelected(false);
            }
            itemComboBox.setValue(null);
            item = new Item(nameField.getText(), weight, size);
        } catch (Exception e) {
            classOne.setDisable(true);
            classTwo.setDisable(true);
            classThree.setDisable(true);
            classOne.setSelected(false);
            classTwo.setSelected(false);
            classThree.setSelected(false);
        }
        
    }
    
    /*
    The two following events update smarpost lists to represent the ones of the selected city.
    The first SmartPost in the list is also automatically selected.
    */
    
    @FXML
    private void checkStartPosts(Event event) {
        ObservableList<SmartPost> spl = FXCollections.observableArrayList();
        for (int i = 0; i < smartposts.getSmartPosts().size(); i++) {
            if (smartposts.getSmartPosts().get(i).getCity().equals(startComboBox.getValue())) {
                spl.add(smartposts.getSmartPosts().get(i));
            }
        }
        startPostComboBox.setItems(spl);
        if (spl.size() != 0) {
            startPostComboBox.setValue(spl.get(0));
        }
        
    }

    @FXML
    private void checkEndPosts(Event event) {
        ObservableList<SmartPost> spl = FXCollections.observableArrayList();
        for (int i = 0; i < smartposts.getSmartPosts().size(); i++) {
            if (smartposts.getSmartPosts().get(i).getCity().equals(endComboBox.getValue())) {
                spl.add(smartposts.getSmartPosts().get(i));
            }
        }
        endPostComboBox.setItems(spl);
        if (spl.size() != 0) {
            endPostComboBox.setValue(spl.get(0));
        }
    }
    
}
