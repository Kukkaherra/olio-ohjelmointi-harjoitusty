/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

/**
 *
 * @author f0388819
 */
public class SmartPost {
    private String city;
    private String code;
    private double lat;
    private double lng;
    private String address;
    private String availability;
    private String postoffice;
    
    public SmartPost(String city, String code, double lat, double lng, String address, String availability, String postoffice) {
        this.city = city;
        this.code = code;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.availability = availability;
        this.postoffice = postoffice;
    }
    
    public String getCity() {
        return city;
    }

    public String getCode() {
        return code;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostoffice() {
        return postoffice;
    }
    
    public String toString() {
        return address;
    }
}
