/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package olioht;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author k8021
 */
public class Storage {
    private static Storage instance = null;
    private ObservableList<Packet> packetlist;

    protected Storage() {
        packetlist = FXCollections.observableArrayList();
    }
    
    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }
    
    public void addPacket(Packet p) {
        packetlist.add(p);
    }
    
    public ObservableList<Packet> getStorage() {
        return packetlist;
    }
}
