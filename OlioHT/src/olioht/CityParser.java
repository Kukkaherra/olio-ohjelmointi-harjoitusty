/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.IOException;
import java.io.StringReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author f0388819
 */
public class CityParser {
    
    private Document doc;
    private ObservableList<String> cities;
    
    
    public CityParser (String content) {
        
        /*
        Code from lecture examples, adapted to work for this project
        */
        
        try {
            cities = FXCollections.observableArrayList();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println("Errori");
        }
        
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place");
        String city = "";
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String name = getValue("city", e);
            if (!name.equals(city)) {
                cities.add(name);
                city = name;
            }
        
        }
        
    
    
    }
    
    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    
    }
    
    public ObservableList<String> getCities() {
        return cities;
    }
}
